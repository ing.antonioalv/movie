<?php
//Headers para prevenir el cache
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
if ($_POST) {

	$p_id=$_POST["p_id"];
	$p_pelicula=$_POST["p_pelicula"];
	$p_popularidad=$_POST["p_popularidad"];
	$p_voto=$_POST["p_voto"];

	include("../class/class_favorita_dal.php");
    $obj_favorita=new favorita();
    $obj_favorita->setid($p_id);
    $obj_favorita->setpelicula($p_pelicula);
    $obj_favorita->setpopularidad($p_popularidad);
    $obj_favorita->setvoto($p_voto);

    $favorita=new favorita_dal();
    $favorita->insert_data($obj_favorita);
    unset($favorita);


	}
?>