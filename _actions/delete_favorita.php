<?php
//Headers para prevenir el cache
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

if ($_POST) {

	$p_id = $_POST['p_id'];
	include("../class/class_favorita_dal.php");
	$favorita = new favorita_dal();

	$favorita->delete_data($p_id);

	unset($favorita);

} 
?>