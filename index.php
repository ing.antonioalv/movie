<!DOCTYPE html>
<html
  lang="en"
  class="light-style layout-menu-fixed"
  dir="ltr"
  data-theme="theme-default"
  data-assets-path="assets/"
  data-template="vertical-menu-template-free"
>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
    />

    <title>Películas</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="assets/img/favicon/favicon.ico" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
      rel="stylesheet"
    />

    <!-- Icons. Uncomment required icon fonts -->
    <link rel="stylesheet" href="assets/vendor/fonts/boxicons.css" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="assets/vendor/css/core.css" class="template-customizer-core-css" />
    <link rel="stylesheet" href="assets/vendor/css/theme-default.css" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="assets/css/demo.css" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />

    <!-- Page CSS -->

    <!-- Helpers -->
    <script src="assets/vendor/js/helpers.js"></script>

   
    <script src="assets/js/config.js"></script>
  </head>

  <body>
    <!-- Layout wrapper -->
    <div class="layout-wrapper layout-content-navbar">
      <div class="layout-container">
        <!-- Menu -->

        <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
          <div class="app-brand demo">
            <a href="index.html" class="app-brand-link">
              <span class="app-brand-logo demo">
                <svg
                  width="25"
                  viewBox="0 0 25 42"
                  version="1.1"
                  xmlns="http://www.w3.org/2000/svg"
                  xmlns:xlink="http://www.w3.org/1999/xlink"
                >
                  <defs>
                    <path
                      d="M13.7918663,0.358365126 L3.39788168,7.44174259 C0.566865006,9.69408886 -0.379795268,12.4788597 0.557900856,15.7960551 C0.68998853,16.2305145 1.09562888,17.7872135 3.12357076,19.2293357 C3.8146334,19.7207684 5.32369333,20.3834223 7.65075054,21.2172976 L7.59773219,21.2525164 L2.63468769,24.5493413 C0.445452254,26.3002124 0.0884951797,28.5083815 1.56381646,31.1738486 C2.83770406,32.8170431 5.20850219,33.2640127 7.09180128,32.5391577 C8.347334,32.0559211 11.4559176,30.0011079 16.4175519,26.3747182 C18.0338572,24.4997857 18.6973423,22.4544883 18.4080071,20.2388261 C17.963753,17.5346866 16.1776345,15.5799961 13.0496516,14.3747546 L10.9194936,13.4715819 L18.6192054,7.984237 L13.7918663,0.358365126 Z"
                      id="path-1"
                    ></path>
                    <path
                      d="M5.47320593,6.00457225 C4.05321814,8.216144 4.36334763,10.0722806 6.40359441,11.5729822 C8.61520715,12.571656 10.0999176,13.2171421 10.8577257,13.5094407 L15.5088241,14.433041 L18.6192054,7.984237 C15.5364148,3.11535317 13.9273018,0.573395879 13.7918663,0.358365126 C13.5790555,0.511491653 10.8061687,2.3935607 5.47320593,6.00457225 Z"
                      id="path-3"
                    ></path>
                    <path
                      d="M7.50063644,21.2294429 L12.3234468,23.3159332 C14.1688022,24.7579751 14.397098,26.4880487 13.008334,28.506154 C11.6195701,30.5242593 10.3099883,31.790241 9.07958868,32.3040991 C5.78142938,33.4346997 4.13234973,34 4.13234973,34 C4.13234973,34 2.75489982,33.0538207 2.37032616e-14,31.1614621 C-0.55822714,27.8186216 -0.55822714,26.0572515 -4.05231404e-15,25.8773518 C0.83734071,25.6075023 2.77988457,22.8248993 3.3049379,22.52991 C3.65497346,22.3332504 5.05353963,21.8997614 7.50063644,21.2294429 Z"
                      id="path-4"
                    ></path>
                    <path
                      d="M20.6,7.13333333 L25.6,13.8 C26.2627417,14.6836556 26.0836556,15.9372583 25.2,16.6 C24.8538077,16.8596443 24.4327404,17 24,17 L14,17 C12.8954305,17 12,16.1045695 12,15 C12,14.5672596 12.1403557,14.1461923 12.4,13.8 L17.4,7.13333333 C18.0627417,6.24967773 19.3163444,6.07059163 20.2,6.73333333 C20.3516113,6.84704183 20.4862915,6.981722 20.6,7.13333333 Z"
                      id="path-5"
                    ></path>
                  </defs>
                  <g id="g-app-brand" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <g id="Brand-Logo" transform="translate(-27.000000, -15.000000)">
                      <g id="Icon" transform="translate(27.000000, 15.000000)">
                        <g id="Mask" transform="translate(0.000000, 8.000000)">
                          <mask id="mask-2" fill="white">
                            <use xlink:href="#path-1"></use>
                          </mask>
                          <use fill="#696cff" xlink:href="#path-1"></use>
                          <g id="Path-3" mask="url(#mask-2)">
                            <use fill="#696cff" xlink:href="#path-3"></use>
                            <use fill-opacity="0.2" fill="#FFFFFF" xlink:href="#path-3"></use>
                          </g>
                          <g id="Path-4" mask="url(#mask-2)">
                            <use fill="#696cff" xlink:href="#path-4"></use>
                            <use fill-opacity="0.2" fill="#FFFFFF" xlink:href="#path-4"></use>
                          </g>
                        </g>
                        <g
                          id="Triangle"
                          transform="translate(19.000000, 11.000000) rotate(-300.000000) translate(-19.000000, -11.000000) "
                        >
                          <use fill="#696cff" xlink:href="#path-5"></use>
                          <use fill-opacity="0.2" fill="#FFFFFF" xlink:href="#path-5"></use>
                        </g>
                      </g>
                    </g>
                  </g>
                </svg>
              </span>
              <span class="app-brand-text demo menu-text fw-bolder ms-2">Películas</span>
            </a>

            <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
              <i class="bx bx-chevron-left bx-sm align-middle"></i>
            </a>
          </div>

          <div class="menu-inner-shadow"></div>

          <ul class="menu-inner py-1">
            <!-- Dashboard -->
            <li class="menu-item">
              <a href="../movie" class="menu-link">
                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                <div data-i18n="Analytics">Búsqueda de Películas</div>
              </a>
            </li>
          </ul>
        </aside>
        <!-- / Menu -->

        <!-- Layout container -->
        <div class="layout-page">
          <!-- Navbar -->

          <nav
            class="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme"
            id="layout-navbar"
          >
            <div class="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
              <a class="nav-item nav-link px-0 me-xl-4" href="javascript:void(0)">
                <i class="bx bx-menu bx-sm"></i>
              </a>
            </div>

            <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
              <!-- Search -->
              <div class="navbar-nav align-items-center">
                <div class="nav-item d-flex align-items-center">
                  <i class="bx bx-search fs-4 lh-0"></i>
                  <input id="buscartext" 
                    type="text"
                    class="form-control border-0 shadow-none"
                    placeholder="Búsqueda de Películas"
                    aria-label="Búsqueda de Películas"
                  />
                </div>
              </div>
              <!-- /Search -->

              <ul class="navbar-nav flex-row align-items-center ms-auto">
                <!-- Place this tag where you want the button to render. -->
                <li class="nav-item lh-1 me-3">
                  
                  <button id="buscar"  type="button" class="btn rounded-pill btn-primary">
                              <span class="tf-icons bx bx-star"></span>&nbsp; Buscar
                            </button>
                </li>

               
              </ul>
            </div>
          </nav>

          <!-- / Navbar -->

          <!-- Content wrapper -->
          <div class="content-wrapper">
            <!-- Content -->

            <div class="container-xxl flex-grow-1 container-p-y">
              <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Búsqueda de Películas /</span> Películas Favoritas</h4>
              <div class="row mb-5" id="fill">              
                
                <!--<div class="col-md-6 col-lg-4">
                  <div class="card">
                    <img class="card-img-top" src="assets/img/elements/7.jpg" alt="Card image cap" />
                    <div class="card-body">
                      <h5 class="card-title">Card title</h5>
                      <p class="card-text">Some quick example text to build on the card title.</p>
                    </div>
                    <ul class="list-group list-group-flush">
                      <li class="list-group-item">Cras justo odio</li>
                      <li class="list-group-item">Vestibulum at eros</li>
                    </ul>
                    <div class="card-body">
                      <a href="javascript:void(0)" class="card-link">Card link</a>
                      <a href="javascript:void(0)" class="card-link">Another link</a>
                    </div>
                  </div>
                </div>-->
              </div>
            </div>
            <!-- / Content -->

            <!-- Footer -->
            <footer class="content-footer footer bg-footer-theme">
              <div class="container-xxl d-flex flex-wrap justify-content-between py-2 flex-md-row flex-column">
                <div class="mb-2 mb-md-0">
                 
                </div>
                <div>
                  
                </div>
              </div>
            </footer>
            <!-- / Footer -->

            <div class="content-backdrop fade"></div>
          </div>
          <!-- Content wrapper -->
        </div>
        <!-- / Layout page -->
      </div>

      <!-- Overlay -->
      <div class="layout-overlay layout-menu-toggle"></div>
    </div>
    <!-- / Layout wrapper -->

    <div class="buy-now">
      <button type="button" class="btn btn-danger btn-buy-now" data-bs-toggle="modal" data-bs-target="#exLargeModal" onclick="reload()" 
        >Ver Películas Favoritas</button>
    </div>

    <div class="modal fade" id="exLargeModal" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog modal-xl" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel4">Películas Favoritas</h5>
                              <button
                                type="button"
                                class="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close"
                              ></button>
                            </div>
                            <div class="modal-body">
                              <div class="table-responsive text-nowrap">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Película</th>
                        <th>Popularidad</th>
                        <th>Votos</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody id="reloadtable" class="table-border-bottom-0">
                      
                     
                    </tbody>
                  </table>
                </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">
                                Cerrar
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>

    <div class="modal fade" id="largeModal" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel3">Detalle de la Película</h5>
                              <button
                                type="button"
                                class="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close"
                                data-bs-toggle="modal" data-bs-target="#exLargeModal"
                              ></button>
                            </div>
                            <div class="modal-body" id="filmodaldetail">
                              
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal" data-bs-toggle="modal" data-bs-target="#exLargeModal">
                                Cerrar
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>

  <div class="modal fade" id="largeModal2" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel32">Detalle de la Película</h5>
                              <button
                                type="button"
                                class="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close"
                              ></button>
                            </div>
                            <div class="modal-body" id="filmodaldetail2">
                              
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">
                                Cerrar
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>

    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="assets/vendor/libs/jquery/jquery.js"></script>
    <script src="assets/vendor/libs/popper/popper.js"></script>
    <script src="assets/vendor/js/bootstrap.js"></script>
    <script src="assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

    <script src="assets/vendor/js/menu.js"></script>
    <!-- endbuild -->

    <!-- Vendors JS -->
    <script src="assets/vendor/libs/masonry/masonry.js"></script>

    <!-- Main JS -->
    <script src="assets/js/main.js"></script>

    <!-- Page JS -->

    <!-- Place this tag in your head or just before your close body tag. -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
    <script type="text/javascript">
      $("#buscar").click(function(event) {
        var genero='';
        if ($("#buscartext").val() != "") {
          $.ajax({
                url: 'https://api.themoviedb.org/3/search/movie',
                type: 'GET',
                dataType: 'json',
                data: {api_key:'f6e10a910e68b5ed533d1a03e2d14603', language:'es-MX',query:$("#buscartext").val()},
            })
          .done(function(data) {  
            //console.log(data.results);
            if (data.value!=0) {
              //var i=0;
              $("#fill").html("");
              $.each(data.results, function(index, val) {
                //console.log(val);
                $.ajax({
                url: 'https://api.themoviedb.org/3/movie/'+val.id+'?',
                type: 'GET',
                dataType: 'json',
                data: {api_key:'f6e10a910e68b5ed533d1a03e2d14603', language:'es-MX'},
        })
        .done(function(data3){
          console.log(data);
          genero='';
          $.each(data3.genres, function(index, val) {
            //console.log(val.name);
            genero += val.name+' ';
          });
        })
        .fail(function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
        });
                $.ajax({
                  url: '_actions/consulta_favorita_id.php',
                  type: 'POST',
                  dataType: 'json',
                  data: {id:val.id},
                })
                .done(function(data2){
                  //console.log(data2);
                  var stringid = "'"+val.id+"'";
                  var stringtitle = "'"+val.title+"'";
                  var stringpopularity = "'"+val.popularity+"'";
                  var stringvote_count = "'"+val.vote_count+"'";
                  if(data2.status==1){
                    

                    $("#fill").append('<div class="col-md-6 col-lg-4"><div class="card"><a onclick="detalle2('+stringid+')" data-bs-toggle="modal" data-bs-target="#largeModal2" type="button"><img class="card-img-top" src="https://image.tmdb.org/t/p/w500/'+val.poster_path+'" alt="Card image cap" /></a><div class="card-body"> <h5 class="card-title">'+val.title+'</h5></div><ul class="list-group list-group-flush"><li class="list-group-item">Genero: '+genero+'</li><li class="list-group-item">Fecha Lanzamiento: '+val.release_date+'</li></ul> <div class="card-body" id="b'+val.id+'"><button type="button" class="btn btn-danger" onclick="elimina('+stringid+','+stringtitle+','+stringpopularity+','+stringvote_count+')"><span class=" tf-icons bx bx-star"></span>&nbsp; Quitar de Favoritos</button></div></div></div>');

                  }else{
                    $("#fill").append('<div class="col-md-6 col-lg-4"><div class="card"><a onclick="detalle2('+stringid+')" data-bs-toggle="modal" data-bs-target="#largeModal2" type="button"><img class="card-img-top" src="https://image.tmdb.org/t/p/w500/'+val.poster_path+'" alt="Card image cap" /></a><div class="card-body"> <h5 class="card-title">'+val.title+'</h5></div><ul class="list-group list-group-flush"><li class="list-group-item">Genero: '+genero+'</li><li class="list-group-item">Fecha Lanzamiento: '+val.release_date+'</li></ul> <div class="card-body" id="b'+val.id+'"><button type="button" class="btn btn-warning" onclick="agrega('+stringid+','+stringtitle+','+stringpopularity+','+stringvote_count+')"><span class="tf-icons bx bx-star"></span>&nbsp; Agregar a Favoritos</button></div></div></div>');
                  }
                  
                })
                .fail(function(xhr, desc, err){
                  console.log(xhr);
                  console.log("Details: " + desc + "\nError:" + err);
                });
               
               // i++;
              });
            } else{

            }

          })
          .fail(function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
            });

        }else{

        }
      });

      function agrega(id,title,popularity,vote_count){
        var stringid = "'"+id+"'";
        var stringtitle = "'"+title+"'";
        var stringpopularity = "'"+popularity+"'";
        var stringvote_count = "'"+vote_count+"'";

        $.ajax({
          url: '_actions/alta_favorita.php',
          type: 'POST',
          dataType: 'html',
          data: {p_id:id, p_pelicula:title, p_popularidad:popularity, p_voto:vote_count},
        })
        .done(function(response){
          //console.log(response);
          if(response==1){
            $("#b"+id).html("");
            $("#b"+id).append('<button type="button" class="btn btn-danger" onclick="elimina('+stringid+','+stringtitle+','+stringpopularity+','+stringvote_count+')"><span class=" tf-icons bx bx-star"></span>&nbsp; Quitar de Favoritos</button>');

          }
        })
        .fail(function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
        });
      }

      function elimina(id,title,popularity,vote_count){
        var stringid = "'"+id+"'";
        var stringtitle = "'"+title+"'";
        var stringpopularity = "'"+popularity+"'";
        var stringvote_count = "'"+vote_count+"'";

        $.ajax({
          url: '_actions/delete_favorita.php',
          type: 'POST',
          dataType: 'html',
          data: {p_id:id},
        })
        .done(function(response){
          //console.log(response);
          if(response==1){
            $("#b"+id).html("");
            $("#b"+id).append('<button type="button" class="btn btn-warning" onclick="agrega('+stringid+','+stringtitle+','+stringpopularity+','+stringvote_count+')"><span class="tf-icons bx bx-star"></span>&nbsp; Agregar a Favoritos</button>');

          }
        })
        .fail(function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
        });
        reload();
      }

      function reload(){
        //var stringid = "'"+id+"'";
        //var stringtitle = "'"+title+"'";
        //var stringpopularity = "'"+popularity+"'";
        //var stringvote_count = "'"+vote_count+"'";

        $.ajax({
          url: '_actions/consulta_favorita.php',
          type: 'POST',
          dataType: 'json'
        })
        .done(function(data){
          //console.log(data);
          $("#reloadtable").html("");
          $.each(data, function(index, val) {
            var stringid = "'"+val.id+"'";
            var stringtitle = "'"+val.pelicula+"'";
            var stringpopularity = "'"+val.popularidad+"'";
            var stringvote_count = "'"+val.voto+"'";
            $("#reloadtable").append('<tr><td><i class="fab fa-angular fa-lg text-danger me-3"></i> <strong>'+val.id+'</strong></td><td>'+val.pelicula+'</td><td><span class="badge bg-primary">'+val.popularidad+'</span></td><td><span class="badge bg-info">'+val.voto+'</span></td><td><div class="dropdown"><button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown"><i class="bx bx-dots-vertical-rounded"></i></button><div class="dropdown-menu"><button type="button" class="dropdown-item" onclick="detalle('+stringid+')" data-bs-toggle="modal" data-bs-target="#largeModal" data-bs-toggle="modal" data-bs-dismiss="modal"><i class="bx bx-edit-alt me-1"></i> Detalles</button><button type="button" class="dropdown-item" onclick="elimina('+stringid+','+stringtitle+','+stringpopularity+','+stringvote_count+')"><i class="bx bx-trash me-1"></i> Eliminar de Favoritos</button></div></div></td></tr>');

          });
          /*if(response==1){
            $("#b"+id).html("");
            $("#b"+id).append('<button type="button" class="btn btn-warning" onclick="agrega('+stringid+','+stringtitle+','+stringpopularity+','+stringvote_count+')"><span class="tf-icons bx bx-star"></span>&nbsp; Agregar a Favoritos</button>');

          }*/
        })
        .fail(function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
        });
      }
      function detalle(id){
        $.ajax({
                url: 'https://api.themoviedb.org/3/movie/'+id+'?',
                type: 'GET',
                dataType: 'json',
                data: {api_key:'f6e10a910e68b5ed533d1a03e2d14603', language:'es-MX'},
        })
        .done(function(data){
          //console.log(data);
          $("#filmodaldetail").html("");
          $("#filmodaldetail").append('<div class="row mb-5"><div class="col-md"><div class="card mb-3"><div class="row g-0"><div class="col-md-4"><img class="card-img card-img-left" src="https://image.tmdb.org/t/p/w500/'+data.poster_path+'" alt="Card image" /></div><div class="col-md-8"><div class="card-body"><h5 class="card-title">'+data.title+'</h5><p class="card-text">'+data.overview+'</p><p class="card-text"><small class="text-muted" id="idiomatext"></small></p></div></div></div></div></div></div>');
          var idioma='';
          var genero='';
          $.each(data.spoken_languages, function(index, val) {
            //console.log(val.name);
            idioma += val.name+' ';
          });
          $.each(data.genres, function(index, val) {
            //console.log(val.name);
            genero += val.name+' ';
          });
          $("#idiomatext").append('Idiomas: '+idioma+'<br>Genero: '+genero);
        })
        .fail(function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
        });
      }

       function detalle2(id){
        $.ajax({
                url: 'https://api.themoviedb.org/3/movie/'+id+'?',
                type: 'GET',
                dataType: 'json',
                data: {api_key:'f6e10a910e68b5ed533d1a03e2d14603', language:'es-MX'},
        })
        .done(function(data){
          //console.log(data);
          $("#filmodaldetail2").html("");
          $("#filmodaldetail2").append('<div class="row mb-5"><div class="col-md"><div class="card mb-3"><div class="row g-0"><div class="col-md-4"><img class="card-img card-img-left" src="https://image.tmdb.org/t/p/w500/'+data.poster_path+'" alt="Card image" /></div><div class="col-md-8"><div class="card-body"><h5 class="card-title">'+data.title+'</h5><p class="card-text">'+data.overview+'</p><p class="card-text"><small class="text-muted" id="idiomatext2"></small></p></div></div></div></div></div></div>');
          var idioma='';
          var genero='';
          $.each(data.spoken_languages, function(index, val) {
            //console.log(val.name);
            idioma += val.name+' ';
          });
          $.each(data.genres, function(index, val) {
            //console.log(val.name);
            genero += val.name+' ';
          });
          $("#idiomatext2").append('Idiomas: '+idioma+'<br>Genero: '+genero);
        })
        .fail(function(xhr, desc, err) {
                console.log(xhr);
                console.log("Details: " + desc + "\nError:" + err);
        });
      }
    </script>
  </body>
</html>
