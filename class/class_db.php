<?php

if (class_exists('class_db')!=true) {
  class class_db
  {
    //localhost
    private static $db_host='localhost';
    private static $db_user='root';
    private static $db_pass='';
  	protected $db_name='movie';
  	protected $query;
  	protected $rows=array();
  	private $conn;

    //Abre la conexion de la base de datos
    function __construct(){
      $this->conn=new mysqli (self::$db_host, self::$db_user,
                  self::$db_pass, $this->db_name);
    }

    //Cierra la conexion de la base de datos
    function __destruct() {
      if (isset($this->conn)) {
        $this->conn->close();
      }      
    }

//Executa el query de forma singular (Usado en Insert Delete Update)
  	protected function execute_single_query() {
  		$this->conn->query($this->query);
      if ($this->conn->affected_rows==1) {
        echo $insertado=1;
      } else
      {
        echo $insertado=0;
      }
      return $insertado;
  	}

//Executa un query para obtener varios resultados (Usado como en listados, tablas, select, etc)
  	protected function get_results_from_query() {
      if($this->rows!=null)
      {
        unset($this->rows);
      }
  		$result=$this->conn->query($this->query);
  		while ($this->rows[] = $result->fetch_assoc());
  		$result->close();
      array_pop($this->rows);
  	}

    protected function realescapestring($str=null){
     $result=$this->conn->real_escape_string($str);
     return $result;
    }
  }
}
?>