<?php
class favorita{
    protected $id;
    protected $pelicula;
    protected $popularidad;
    protected $voto;

    public function setid($id) { $this->id = $id; }
    public function getid() { return $this->id; }

    public function setpelicula($pelicula) { $this->pelicula = $pelicula; }
    public function getpelicula() { return $this->pelicula; }

    public function setpopularidad($popularidad) { $this->popularidad = $popularidad; }
    public function getpopularidad() { return $this->popularidad; }

    public function setvoto($voto) { $this->voto = $voto; }
    public function getvoto() { return $this->voto; }

}
?>