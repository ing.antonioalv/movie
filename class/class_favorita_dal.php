<?php
include_once "class_favorita.php";
include_once "class_db.php";

class favorita_dal extends class_db{

    function __construct()
    {
        parent:: __construct();
    }

    function __destruct()
    {
        parent:: __destruct();
    }

    function main_list(){

        $this->query="SELECT *
                        FROM favorita";
        $this->get_results_from_query();
        $i=1;
        $lista=NULL;
        foreach ($this->rows as $key => $value) {
            $obj = new favorita;
            $obj->setid(utf8_encode($value["id"]));  
            $obj->setpelicula(utf8_encode($value["pelicula"]));
            $obj->setpopularidad(utf8_encode($value["popularidad"]));
            $obj->setvoto(utf8_encode($value["voto"]));
            $lista[$i]=$obj;
            unset($obj);
            $i++;
        }
        return $lista;
    }

    public function list_by_id($id) {
        $this->query="SELECT *
                        FROM favorita
                        WHERE id='$id'";
        $this->get_results_from_query();
        $i=1;
        $lista=NULL;
        foreach ($this->rows as $key => $value) {
            $obj = new favorita;
            $obj->setid(utf8_encode($value["id"]));  
            $obj->setpelicula(utf8_encode($value["pelicula"]));
            $obj->setpopularidad(utf8_encode($value["popularidad"]));
            $obj->setvoto(utf8_encode($value["voto"]));
            $lista[$i]=$obj;
            unset($obj);
            $i++;
        }
        return $lista;
    }

    public function insert_data($obj_data) {
        $id=$this->realescapestring(utf8_decode(mb_strtoupper($obj_data->getid())));
        $pelicula=$this->realescapestring(utf8_decode(mb_strtoupper($obj_data->getpelicula())));
        $popularidad=$this->realescapestring(utf8_decode(mb_strtoupper($obj_data->getpopularidad())));
        $voto=$this->realescapestring(utf8_decode(mb_strtoupper($obj_data->getvoto())));

        $this->query="INSERT INTO favorita
        (
            id, 
            pelicula, 
            popularidad, 
            voto
        )
        VALUES (
            '$id',
            '$pelicula',
            '$popularidad',
            '$voto'
        )";
        //print_r($this->query);
        $this->execute_single_query();
    }

    public function delete_data($id){

      $sql = "Delete from favorita where id ='$id'";

      $this->query=$sql;
      $this->execute_single_query();
      
    }
}
?>